use chrono::prelude::*;
use cidr::{Cidr, IpCidr};
use std::convert::TryFrom;
use std::fmt;
use std::fs::{self, File};
use std::io::{BufRead, Write};
use std::path::{Path, PathBuf};
use std::process::Command;

mod roafilter;
use roafilter::*;

#[derive(Debug)]
struct ROA {
    route: IpCidr,
    origins: Vec<u32>,
    maxlen: u8,
}

impl fmt::Display for ROA {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        /* we want /32 and /128 */
        let lineprefix = format!(
            "route {}/{} max {} as ",
            self.route.first_address(),
            self.route.network_length(),
            self.maxlen,
        );
        for origin in &self.origins {
            writeln!(f, "{}{};", lineprefix, origin)?;
        }

        Ok(())
    }
}

impl TryFrom<File> for ROA {
    type Error = &'static str;

    fn try_from(file: File) -> Result<Self, Self::Error> {
        let mut attributes: Vec<(String, String)> = vec![];

        for wrapped_line in std::io::BufReader::new(file).lines() {
            let line = wrapped_line.unwrap();

            /* is this a continuation of the previous attribute ? */
            if line.starts_with(' ') {
                if attributes.is_empty() {
                    return Err("Invalid route object starting with whitespace");
                }

                let (_, ref mut v) = attributes.last_mut().unwrap();
                v.push_str(line.trim_start());
            }

            /* newline continuation */
            if line.starts_with('+') {
                if attributes.is_empty() {
                    return Err("Invalid route object starting with a newline continuation");
                }

                let (_, ref mut v) = attributes.last_mut().unwrap();
                v.push('\n');
            }

            let fields: Vec<&str> = line.splitn(2, ' ').collect();
            if fields.is_empty() {
                return Err("Invalid route object");
            }

            attributes.push((
                    (*fields.get(0).unwrap()).to_string(),
                    fields.get(1).unwrap_or(&"").trim_start().to_string()
                    ));
        }


        let mut route: Option<IpCidr> = None;
        let mut origins: Vec<u32> = Vec::new();
        let mut maxlen: Option<u8> = None;

        for (key, value) in attributes {
            match key.as_ref() {
                "route:" | "route6:" => {
                    let v: IpCidr = value.parse::<IpCidr>().map_err(|_| "invalid route attribute")?;
                    route = Some(v);
                },
                "origin:" => {
                    let origin: u32 = value.trim_start_matches("AS").parse().map_err(|_| "invalid origin attribute")?;
                    origins.push(origin);
                },
                "max-length:" => {
                    maxlen = Some(value.parse().map_err(|_| "invalid max-length attribute")?);
                },
                _ => {}
            }
        }

        if origins.is_empty() {
            return Err("missing origin attributes");
        }

        Ok(Self {
            route: route.expect("missing route attribute"),
            origins,
            maxlen: maxlen.unwrap_or(0),
        })
    }
}

/* yeah we don't really distinguish between ipv4 / ipv6 ... */
fn parse_roa(path: PathBuf) -> Vec<ROA> {
    fs::read_dir(path)
        .expect("couldn't access route objects")
        .filter_map(|route| {
            let path = route.unwrap().path();
            let f = File::open(path).expect("could not access route object");
            ROA::try_from(f)
                .map_err(|e| eprintln!("error while parsing roa: {}", e))
                .ok()
        })
        .collect()
}

fn roa_filter(roas: Vec<ROA>, filters: Vec<ROAFilter>) -> Vec<ROA> {
    roas.into_iter()
        .filter_map(|roa| {
            /* map every roa to it's matching roa filter */
            for filter in &filters {
                if filter.prefix.contains(&roa.route.first_address()) {
                    return Some((roa, filter));
                }
            }

            eprintln!("# warning: dropped {:?}", roa);
            None
        })
        .filter(|(roa, filter)|
        /* drop roa if the filter isn't permit */
        match filter.action {
            ROAFilterAction::Permit => true,
            ROAFilterAction::Deny => {
                eprintln!("# warning: dropped {:?}", roa);
                false
            }
        })
        .filter_map(|(roa, filter)| {
            if roa.route.network_length() > filter.maxlen {
                eprintln!("# warning: dropped {:?}", roa);
                return None;
            }

            let maxlen = if roa.maxlen != 0
                && (roa.maxlen < filter.maxlen)
                && (roa.maxlen > filter.minlen)
            {
                roa.maxlen
            } else {
                filter.maxlen
            };

            Some(ROA {
                route: roa.route,
                origins: roa.origins,
                maxlen,
            })
        })
        .collect()
}

fn process_ipv(
    data_path: &Path,
    output_dir: &Path,
    date: DateTime<Utc>,
    git_commit: &str,
    filter_file: &str,
    route_dir: &str,
    roa_file: &str,
) {
    let roa_filter_ = parse_filter(
        File::open(data_path.join(filter_file))
            .unwrap_or_else(|_| panic!("could not read {}", filter_file)),
    );
    let roa_raw = parse_roa(data_path.join(route_dir));
    let mut roa_fd = File::create(output_dir.join(roa_file))
        .unwrap_or_else(|_| panic!("could not open {} output file", roa_file));

    // NOTE: git_commit ends with a newline
    writeln!(
        &mut roa_fd,
        "#\n# dn42-roagen: simple dn42 roa generator\n# generated on: {}\n# commit: {}#",
        date, git_commit
    )
    .expect("failed to write header");

    let mut roas = roa_filter(roa_raw, roa_filter_);
    roas.sort_by(|a, b| a.route.first_address().cmp(&b.route.first_address()));
    for roa in roas {
        write!(roa_fd, "{}", roa.to_string()).unwrap();
    }
}

fn main() {
    use clap::Arg;
    let matches = clap::App::new("dn42-roagen")
        .about("Generate bird roa files from the dn42 registry data")
        .version(clap::crate_version!())
        .arg(
            Arg::with_name("registry_path")
                .takes_value(true)
                .multiple(false)
                .index(1)
                .required(true)
                .help("path to a checked-out branch of the DN42 registry"),
        )
        .arg(
            Arg::with_name("output_dir")
                .takes_value(true)
                .multiple(false)
                .index(2)
                .required(true)
                .help("path to the output directory"),
        )
        .get_matches();

    let registry_path: PathBuf = matches.value_of_os("registry_path").unwrap().into();
    let output_dir: PathBuf = matches.value_of_os("output_dir").unwrap().into();
    let data_path = registry_path.join("data");

    let date = Utc::now();
    let git_commit_output = Command::new("git")
        .arg("-C")
        .arg(&registry_path)
        .arg("log")
        .arg("-1")
        .arg("--format=%H")
        .output()
        .expect("failed to determinte git commit")
        .stdout;
    let git_commit = String::from_utf8_lossy(&git_commit_output);

    process_ipv(
        &data_path,
        &output_dir,
        date,
        &git_commit,
        "filter.txt",
        "route",
        "dn42-roa4.conf",
    );
    process_ipv(
        &data_path,
        &output_dir,
        date,
        &git_commit,
        "filter6.txt",
        "route6",
        "dn42-roa6.conf",
    );
}
