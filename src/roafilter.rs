use cidr::IpCidr;
use std::convert::TryFrom;
use std::fs::File;
use std::io::BufRead;
use std::str::FromStr;

#[derive(Debug, PartialEq)]
pub enum ROAFilterAction {
    Permit,
    Deny,
}

impl FromStr for ROAFilterAction {
    type Err = &'static str;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "permit" => Ok(Self::Permit),
            "deny" => Ok(Self::Deny),
            _ => Err("invalid action"),
        }
    }
}

#[derive(Debug)]
pub struct ROAFilter {
    pub number: u16,
    pub action: ROAFilterAction,
    pub prefix: IpCidr,
    pub minlen: u8,
    pub maxlen: u8,
    // pub network: String,
}

impl TryFrom<Vec<&str>> for ROAFilter {
    type Error = &'static str;
    fn try_from(fields: Vec<&str>) -> Result<Self, Self::Error> {
        if fields.len() < 5 {
            return Err("got invalid roa filter line");
        }
        Ok(Self {
            number: fields[0].parse().map_err(|_| "Invalid number")?,
            action: fields[1].parse().map_err(|_| "invalid action")?,
            prefix: fields[2].parse().map_err(|_| "invalid prefix")?,
            minlen: fields[3].parse().map_err(|_| "invalid minlen")?,
            maxlen: fields[4].parse().map_err(|_| "invalid maxlen")?,
        })
    }
}

pub fn parse_filter(file: File) -> Vec<ROAFilter> {
    let mut results = std::io::BufReader::new(file)
        .lines()
        .map(Result::unwrap)
        .filter_map(|mut real_line| {
            if let Some(i) = real_line.find('#') {
                real_line.truncate(i);
            }

            let fields: Vec<&str> = real_line.split_whitespace().collect();
            if fields.is_empty() {
                return None;
            }

            ROAFilter::try_from(fields)
                .map_err(|e| {
                    eprintln!("parse_filter: (line = {}): {}", real_line, e);
                })
                .ok()
        })
        .collect::<Vec<_>>();

    /* sort by number */
    results.sort_by(|a, b| a.number.cmp(&b.number));

    results
}
