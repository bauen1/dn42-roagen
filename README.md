# dn42-roagen

Generate bird 2.0 roa entries from the dn42 registry.

## Build

1. Install `cargo-deb`: `cargo install cargo-deb`
2. Build dpkg: `cargo deb`

## Usage

After Installtion add the ssh public key from `/etc/dn42-roagen/id_ed25591.pub` to your account on [git.dn42.dev](https://git.dn42.dev/)

Add
```bird

roa4 table dn42_roa4;
roa6 table dn42_roa6;

protocol static {
    roa4 {
        table dn42_roa4;
    };
    include "/var/lib/bird/dn42-roa4.conf";
};

protocol static {
    roa6 {
        table dn42_roa6;
    };
    include "/var/lib/bird/dn42-roa6.conf";
};
```
to your bird config.
You can now use
```
if (roa_check(dn42_roa4, net, bgp_path.last) != ROA_VALID) then {
    reject;
}

if (roa_check(dn42_roa6, net, bgp_path.last) != ROA_VALID) then {
    reject;
}
```
to reject any routes without a matching and valid roa entry.
